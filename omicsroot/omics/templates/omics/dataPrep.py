# This file reads in the Primary Data Set (PDS) of diseased cells and the Reference Tissue Set (RTS).
# They are processed to have the same normalization so they can be compared.
# An initial sample of 200 rows is produced for creating and testing the downstream workflow.

import csv
import gc
import numbers
from datetime    import datetime
from collections import OrderedDict
from itertools   import chain

# RTS processing
#################################################################
# process and normalize sample tissues file
# drop first two rows
# drop entrez name gene column
localPath  = 'D:\\Google Drive\\DePaulGraduateSchool\\CSC529\\'
rtsfile    = 'All_Tissue_Site_Details.combined.reads.gct'
fileToRead = localPath + rtsfile

pdsfile = 'Sample1_Infected_5000UMI.csv'
pdsfileToRead = localPath + pdsfile

skipRows    = 3 # skip this many iunitial rows
limitToRead = 100000 # process this many after skips
colLimit    = 500
rowsRead    = 0
tossed      = 0
totalRows   = 0

geneList          = []
geneNames         = []
infectedGeneNames = []

start = datetime.today()
print(start)

# get gene list from infected file
with open(pdsfileToRead, 'r') as pdstissueFile:
    rowreader = csv.reader(pdstissueFile, delimiter=',', quotechar='"')
    next(rowreader)
    for row in rowreader:
        infectedGeneNames.append(row[0])
        #print(row[0])
#print(infectedGeneNames)

def getPDS():
    # get genes from infected file
    infectedGenes = {}
    print('Reading PDS')
    with open(pdsfileToRead, 'r') as pdstissueFile:
        rowreader = csv.reader(pdstissueFile, delimiter=',', quotechar='"')
        next(rowreader) # skip first row of titles
        for row in rowreader:
            #infectedGenes.append(row)
            #print(row[1:])
            infectedGenes[row[0]] = [int(y) for y in row[1:]] # need the hash lookup
    print('Finished reading PDS')
    return infectedGenes

def samplesize(listy):
    print("r= " + str(len(listy)) + " col= " + str(len(listy[1])))

#infectedGenes = getPDS()
#print(list(infectedGenes.keys())[0])
#print(len(infectedGenes[list(infectedGenes.keys())[0]]))
#samplesize(infectedGenes)




with open(fileToRead, 'r') as tissueFile:
    rowreader = csv.reader(tissueFile, delimiter='\t', quotechar='|')
    for row in rowreader:
        totalRows += 1
        if(skipRows > 0):
            print('skipped row')
            skipRows -= 1
        else:
            gene = row[1]
            #print(gene)
            if(gene not in infectedGeneNames):
                tossed +=1
                #print('skipped gene: ' + gene)
            else:
                if( (limitToRead % 1000) == 0):
                    print('limitToRead = ' + str(limitToRead))
                    print('Length of rows:' + str(len(row)))
                #if(row[1] == 'Y_RNA'):
                #    print(row)
                if(limitToRead > 0 ):
                    #print(row)
                    #print( ', '.join(row))
                    currentRow = list(row)
                    currentRow = currentRow[:colLimit]
                    #print('Length of rows:' + str(len(currentRow)))
                    #convert to numeric here and create matrix
                    #some entries have scientific notation so I convert to that first - then to int
                    valuesTrans = [float(y) for y in currentRow[2:]]
                    geneNamen = currentRow[:2]
                    valuesTrans = geneNamen + [int(y) for y in valuesTrans]
                    #geneNamen   = currentRow[:2]
                    #print(valuesTrans)
                    #print(geneNamen)
                    geneList.append(valuesTrans)
                    geneNames.append(geneNamen)
                    rowsRead += 1
                    if(rowsRead % 1000 == 0):
                        print("gc'ing")
                        gc.collect()
                    #print(currentRow)
                else:
                    break;
                limitToRead -= 1

print(geneList[1]) #print confidence sample
print(len(geneList))

print(geneNames[1])
print(len(geneNames))

print('Skipped ' + str(tossed) + ' genes out of ' + str(totalRows) + ' rows')
#pull out values only (represent UMIs)
#values   = [sublist[3:] for sublist in geneList]

#for rowCheck in (sublist[3:] for sublist in geneList):
#    print(rowCheck)
#print(len(geneList))
# pull out rownames to join back later
#rowNames = [sublist[:2] for sublist in geneList]
#print(values)
#print(rowNames[:3])


#for x in sorted([x for y,x in geneNames]):
#    print(x)

# write out genes in this file
# #################################################################
with open(localPath + 'geneNames.csv', 'w', newline='') as file:
     writer = csv.writer(file)
     writer.writerows(geneNames)

print('RDS gene names Data Written Out')

# geneListCombined = []
# # combine lists back
# for i in range(len(geneList)):
#     #print(geneList[i])
#     #print(geneNames[i])
#     newRow = geneNames[i] + geneList[i]
#    # newRow.append(geneList[i])
#     #print(newRow)
#     geneListCombined.append(newRow)
#
# print(geneListCombined[1])

#
# look for repeated Genes - saw this problem trying to load this into a named matrix in R
commonNames  = [ gene[1] for gene in geneNames]
#print(geneList[:10])
geneCount = [ [gene, geneList.count(gene)] for gene in commonNames]
print(geneCount[:10])
samplesize(geneList)

uniqueGeneCount = {}
for row in geneCount:
     uniqueGeneCount[row[0]] = row[1]
     #print(uniqueGeneCount)

print('Repeated Gene List:')
dropList = []
for (key,value) in uniqueGeneCount.items():
    #print(key + ':' + str(value))
    if(value > 1):
        print('To drop:' + key + ':' + str(value))
        dropList.append(key)
#print(dropList)
print ('***********************')

print('Deduping')
def deDupe(iterable,dropList):
    for x in iterable:
        #print(x[1] )
        if x[1] not in dropList:
            yield x
        else:
            print('Dropped :' + x[0] + ':' + x[1])

deDupped = list(deDupe(geneList, dropList))
#print(deDupped[:1])

deDupped.sort(key=lambda x: x[1])
geneListCombined = deDupped
print('Gene list combined size now:')
samplesize(geneListCombined)
del deDupped
gc.collect()

# unnormalized set
#################################################################
with open(localPath + rtsfile + '.combined', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(geneListCombined)

print('RTS Data Written Out')


# PDS processing
#################################################################
# process and normalize infected UMIs
# normalize by column
# write out normalized full data set
#################################################################

infectedGenes = getPDS() # get dictionary of terms for easy lookup
#print(list(infectedGenes.keys())[0])
#print(len(infectedGenes[list(infectedGenes.keys())[0]]))
print('*********PDS size *************')
print("r= " + str(len(infectedGenes.keys())) + " col= " + str(len(infectedGenes[list(infectedGenes.keys())[0]])))
print('********* RTS size *************')
samplesize(geneListCombined)

#combined two sets driven by now dedupped RTS
geneListAllSamples = []
for x in geneListCombined:
    #print(x[1])
    key = x[1]
    if(key not in infectedGenes.keys()):
        #print('Gene:' + key + ' not found in infected samples')
        break
    else:
        #print(infectedGenes[x])
        geneListAllSamples.append( x[0:2] + (infectedGenes[key])  + x[3:])

print(geneListAllSamples[1])
print('GeneListAllSamples size')
samplesize(geneListAllSamples)

# normalize by column
valuesTrans   = [list(i) for i in zip(*geneListAllSamples)] # transpose to make it easy to normalize each sublist
print("transposed size =")
samplesize(valuesTrans)
geneList = []
gc.collect()

# normalize
# naive pythonic way is really slow
#valuesTrans = [[ (y - min(row)) / (max(row) - min(row)) for y in row] for row in valuesTrans]

newValuesList = []
for row in valuesTrans:
    # ignore lists of anything but numbers - hacky hacky hacky
    if(isinstance(row[0], numbers.Number)):
        #print('Normo one')
        minny  = min(row)
        maxxy  = max(row)
        denom  = (max(row) - min(row))
        newRow = []
        for value in row:
            newRow.append( round((value - minny) / denom,6))
        #print('Row Normalized')
        #print(newRow)
        newValuesList.append(newRow)
    else:
        print('String column ? :' + row[0])
        newValuesList.append(row) # just append if not processed

del valuesTrans
gc.collect()

print('Normalization Complete')

geneList = [list(i) for i in zip(*newValuesList)] # transpose back
print('transposed back size =')
samplesize(geneList)

del newValuesList
gc.collect()

print('Adding labels as columns for tissue samples')
labels = ['entrez','common']
labels.extend(['PDS,']*(len(geneList[0])-2-colLimit))
labels.extend(['RTS']*colLimit)
#print(labels)

geneList.insert(0, labels)
#valuesToCheck  = [sublist[1] for sublist in newValuesList]
#print('Transpose back good:'+ str( valuesToCheck == valuesTrans2[1]))

for i in range(0,10):
    print(geneList[i])

print(len(geneList[0]))
print(len(geneList[1]))

gc.collect()


# write out normalized full data set gene-wise
#################################################################
with open(localPath + rtsfile + '.normal.gene', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(geneList) # some elements are written in scientific notation
print('Combined Normalized Data Written Out Gene-wise')


# write out normalized full data set label-wise
#################################################################
geneListG = [list(i) for i in zip(*geneList)] # transpose back
with open(localPath + rtsfile + '.normal.label', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(geneListG) # some elements are written in scientific notation
print('Combined Normalized Data Written Out Label-wise')


# Small Set processing
#################################################################
# create a small working sample from each and combine
# read first 100 rows of RTS
# read first 100 rows of PDS
# write out normalized small data set
#################################################################



# Create subsample files
# Since these files are so large I create subfiles by bootstrapping from the two sources and then combining these into the bootstraped sample files
# Since the class imbalance problem is large here - the RTS ssample set is much smaller than the PDS (Gene counts about the same but samples are actually in the columns)
#################################################################
# Create X subsampled files
# for i = 1 to X
#   create subsample PDS
#   create subsample RTS
#   combine
#   Write out file
#################################################################

end = datetime.today()
print(end)
print(end-start)
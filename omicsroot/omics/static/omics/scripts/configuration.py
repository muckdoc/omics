#%%
########################
# multiprocessing switch
########################
# python boolean
multiprocess = True

########################
# paths
########################
# specify project root path(where "omicsroot/" is located), python string
root_path = ""
# python string
cluster_data_path =  "/omicsroot/omics/static/omics/data/hg19.csv"
# specify R interpreter(where "Rscript" is located), python string
R_Path = '/usr/bin/'
# save log file?
save_log_file = True

########################
# experiment input parameters
# used by CreateClusterDistMatrix.py
########################
# cluster data has header? python boolean
input_header = True
# cluster data has rownames? python boolean
input_rownames = True
# python list of floats 
#res_levels = [0.6, 0.8, 1.0, 1.2, 1.4]
res_levels = [0.6, 0.8, 1.0]
# python list of integers 
cluster_dims = [15]
# python list of integers 
ngene_low_limits = [200]
# python list of integers 
ngene_up_limits = [2500]
# python list of floats and(or) strings(to handle infinite) 
perc_mito_low_limits = ["-Inf"]
# python list of floats and(or) strings(to handle infinite) 
#perc_mito_up_limit = [0.05, 0.04]
perc_mito_up_limits = [0.05]
# pythong list of strings 
test_types = ["roc"]

# python float
markers_min_pct = 0.25
# python float
markers_thresh_use = 0.25
# python boolean
filter_colsums_5000 = False

# TODO
filter_percs = [97]
# python float
sig_level = 0.05

########################
# functionality switches
# used by CreateClusterDistMatrix.py
########################
# python boolean
print_log = True
# python boolean
save_cluster_data = True
# python boolean
log_to_file = True


########################
# functionality switches
# used by CreateClusterDistMatrix_Test.py
########################
# python boolean
save_graphml = True
# python boolean
save_hist = True

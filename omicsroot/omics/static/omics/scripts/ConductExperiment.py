# %%
# Read variables from configuration file
from configuration import *
import datetime
import subprocess
import os
import time
import psutil
import json
import pandas as pd
import numpy as np
import logging
if(save_log_file):
    log_file_name = "pipeline_log"
    logging.basicConfig(filename = log_file_name,
    filemode = "a",
    format = "%(asctime)s, %(msecs)d %(name)s %(levelname)s %(message)s ",
    datefmt = "%H:%M:%S",
    level = logging.DEBUG)
else:
    logging.basicConfig(level = logging.DEBUG)


# Get current time and make directories
curr_datetime = str(datetime.datetime.now())[:-7].replace(" ","_").replace("-","_").replace(":","_")
result_path = root_path + 'omicsroot/omics/static/omics/results/'
experiment_path = root_path + 'omicsroot/omics/static/omics/results/' + curr_datetime + "_Bicluster_Results/"
os.mkdir(experiment_path)


# %%
logging.info("Start grid searach...")
##########
# Perform grid search, calls PerformBicluster.R
##########
# Two lists to hold Popen objects and their names
list_of_popen_names = []
list_of_popens = []
loop_count = 0
max_allowed_subprocess = 1

# The switch to turn on memory capability evaluation
first_round = True

base_memory_usage = psutil.virtual_memory()[2]
logging.info("Free memory for use: " + str( round(100 - base_memory_usage, 2) ) + "%")
if(first_round):
    logging.info("First loop: calculating maximum number of subprocesses allowed...")

for res_level in res_levels:
    for cluster_dim in cluster_dims:
        for ngene_low_limit in ngene_low_limits:
            for ngene_up_limit in ngene_up_limits: 
                for perc_mito_low_limit in perc_mito_low_limits:
                    for perc_mito_up_limit in perc_mito_up_limits:            
                        for test_type in test_types:
                            logging.info("Now in loop " + str(loop_count) )

                            while(sum([p.poll() == None for p in list_of_popens]) == max_allowed_subprocess):
                                if (not first_round):
                                    logging.info("Max number of subprocesses: " + str(max_allowed_subprocess) + " is reached, pending for memory recycling...")
                                time.sleep(5)

                            bicluster_cmd = []
                            
                            exp_params = [cluster_data_path,
                            str(int(input_header)), str(int(input_rownames)), 
                            str(res_level), str(cluster_dim),
                            str(ngene_low_limit), str(ngene_up_limit),
                            str(perc_mito_low_limit), str(perc_mito_up_limit),
                            str(test_type),
                            str(markers_min_pct), str(markers_thresh_use),
                            str(int(filter_colsums_5000))]
                            
                            bicluster_cmd += [R_Path + "Rscript", root_path + "omicsroot/omics/static/omics/scripts/PerformBicluster.R"]
                            bicluster_cmd += exp_params
                            bicluster_cmd += [str(loop_count)]
                            bicluster_cmd += [root_path]

                            list_of_popen_names.append(bicluster_cmd)
                            list_of_popens.append(subprocess.Popen(bicluster_cmd))
                            # calculate the max number of subprocesses allowed
                            if(first_round):
                                max_usage = 0
                                while(None in [obj.poll() for obj in list_of_popens]):
                                    # update summit memory usage every 2 seconds
                                    if(psutil.virtual_memory()[2] - base_memory_usage > max_usage):
                                        max_usage = psutil.virtual_memory()[2] - base_memory_usage
                                        logging.info("First loop: single subprocess memory usage updated:" + str(round(max_usage, 2)) + "%" )
                                    time.sleep(5)
                                
                                if(multiprocess):
                                    max_allowed_subprocess = int((100 - base_memory_usage)/max_usage)
                                    logging.info("multiprocessing enabled, max_allowed_subprocess is updated to: " + str(max_allowed_subprocess))
                                else:
                                    logging.info("multiprocessing disabled, max_allowed_subprocess is fixed to 1")
                                
                                first_round = False
                            
                            loop_count += 1

# Wait for the subprocesses to finish
while None in [obj.poll() for obj in list_of_popens]:
    logging.info(str( sum([p.poll() == None for p in list_of_popens]) ) + " subprocesses remaining...")
    time.sleep(5)
logging.info("Grid search finished")
# Put json objects into a list by fetching the json files
bicluster_dicts = []
for i in range(loop_count):
    with open(root_path + "omicsroot/omics/static/omics/data/PB" + str(i) + '.json') as f:
        bicluster_dicts.append(json.load(f))

gene_cluster_dfs = [pd.DataFrame(bicluster_dict['gene_cluster_data']) for bicluster_dict in bicluster_dicts]
# Convert data type
for gene_cluster_df in gene_cluster_dfs:
    gene_cluster_df['cluster'] = gene_cluster_df['cluster'].astype('int')


#%%
logging.info("Find enriched genes per cluster...")
##################################
# Find enriched genes per cluster, calls GetEnrichedGO.R
##################################
cluster_dict = {}
for gene_df_label, gene_cluster_df in enumerate(gene_cluster_dfs):
    logging.info("working on gene_df_label: " + str(gene_df_label) )
    # Find enriched genes per cluster
    cluster_labels = sorted(list(gene_cluster_df['cluster'].unique()))
    for cluster_label in cluster_labels:
        logging.info("cluster_label: " + str(cluster_label))
        # Get data and list of genes belonging to current cluster
        temp_df = gene_cluster_df[gene_cluster_df['cluster'] == cluster_label]
        cluster_gene_list = list(temp_df['gene'])
    
        # Build subprocess command
        enriched_cmd = []
        enriched_cmd += [R_Path + "Rscript", root_path + "omicsroot/omics/static/omics/scripts/GetEnrichedGO.R"]
        enriched_cmd += [str(sig_level)]
        enriched_cmd += cluster_gene_list
        
        # Call GetEnrichedGO.R function
        # check_output will run the command and store to result
        enriched_dict = json.loads(subprocess.check_output(enriched_cmd, universal_newlines = True))
        
        # Build dictionary with list of genes per cluster as each value.
        cluster_dict[str(gene_df_label) + '_' + str(cluster_label)] = list(enriched_dict['enriched_genes'])

# save cluster dictionary as txt
output_rule_path = experiment_path + curr_datetime +  "_Bicluster_Rules_Dict.txt"
f = open(output_rule_path, 'w')
f.write(json.dumps(cluster_dict))
f.close()
logging.info("Bicluster rules: text file generated")

# %%
logging.info("Create cluster dist matrix...")
#################
# Create cluster dist matrix
#################
num_iter = 0
for cluster_key in list(cluster_dict.keys()):
    curr_genes = list(cluster_dict[cluster_key])
    num_enriched_genes = len(curr_genes)
    
    # If the first iteration, create arrays and dataframes to be 
    # populated
    if num_iter == 0:   
        dist_df = pd.DataFrame( np.zeros( (num_enriched_genes, num_enriched_genes), dtype = int) )
        dist_df.columns = curr_genes
        dist_df.index = curr_genes
        
    # Else find the list of genes not present in the current dataframe, 
    # then add columns and rows to the dataframe for each
    else:
        dist_genes = list(dist_df.columns.values)
        
        unmatched_genes = list(set(curr_genes) - set(dist_genes))
        
        if len(unmatched_genes) > 0:
            for gene in unmatched_genes:
                # Get current dataframe row and column sizes
                dist_rows = dist_df.shape[0]
                dist_cols = dist_df.shape[1]
                dist_index = list(dist_df.index)
                
                # Create temp column and row of all zeros 
                temp_col = np.zeros((dist_rows, 1), dtype = int)
                num_new_cols = dist_cols + 1
                temp_row = np.zeros((1, num_new_cols), dtype = int)
                
                # Add temp column and row to existing dataframe 
                dist_df[gene] = pd.DataFrame(temp_col, 
                        index = dist_index)
                new_cols = list(dist_df.columns)
                new_row_df = pd.DataFrame(temp_row, columns = new_cols)
                new_row_df.index = [gene]
                dist_df = pd.concat([dist_df, new_row_df])
                
    # Increment each cell corresponding to a gene pair by 1 (unless  
    # same gene is row and column)
    for row_gene in curr_genes:
        for col_gene in curr_genes:
            if row_gene == col_gene:
                continue
            
            dist_df.loc[row_gene, col_gene] = \
            dist_df.loc[row_gene, col_gene] + 1
    
    num_iter += 1

# Save distance matrix
output_dist_path = experiment_path + curr_datetime + "_Bicluster_Distance_Matrix.csv"
dist_df.to_csv(output_dist_path, header = True, index = True)
logging.info("Bicluster distance matrix: csv file generated")


# %%
logging.info(" Create node_attr_df...")
####################################
# Create node_attr_df, calls CreateFilteredAdjacency.R
####################################
for filter_perc in filter_percs:
    adj_filename = experiment_path + "_Filtered_" + str(filter_perc) + "_Adj_Matrix.csv"
    node_filename = experiment_path + "_Filtered_" + str(filter_perc) + "_Node_Attrs.csv"
    # Build subprocess command
    adj_cmd = []
    adj_cmd += [R_Path + "Rscript", root_path + "omicsroot/omics/static/omics/scripts/CreateFilteredAdjacency.R"]
    adj_cmd += [output_dist_path, adj_filename, str(filter_perc), str(int(save_graphml)), str(int(save_hist))]
    # Call CreateFilteredAdjacency.R
    node_attr_json = subprocess.check_output(adj_cmd, universal_newlines = True)
    # Convert json to data frame
    node_attr_df = pd.DataFrame(json.loads(node_attr_json))
    num_nodes = len(node_attr_df["names"])
    # Save node_attr_df to csv
    node_attr_df.to_csv(experiment_path + curr_datetime + "_Filtered_" + str(filter_perc) + "_Node_Attrs.csv", header = True, index = True)  

    # Convert csv to json
    csvPath = adj_filename[:adj_filename.rfind("_Adj")] + "_" + str(num_nodes) + "_Nodes" + adj_filename[adj_filename.rfind("_Adj"):] 
    df = pd.read_csv(csvPath, index_col = 0, header = 0)

    outputdata = {}
    outputdata["nodes"] = []
    outputdata["links"] = []

    for col in df.columns:
        outputdata["nodes"].append({"id":col,"group":1})
        for row in df.index:
            if df.loc[row, col] != 0:
                outputdata["links"].append({"source":col,"target":row,"value":int(df.loc[row, col])})
    
    json_path = experiment_path + curr_datetime + csvPath[csvPath.rfind("/")+1:].replace(".csv", ".json")
    # save json as file 
    with open(json_path, 'w') as outfile:
        json.dump(outputdata, outfile)
    with open(result_path + curr_datetime + csvPath[csvPath.rfind("/")+1:].replace(".csv", ".json"), "w") as outfile:
        json.dump(outputdata, outfile)

    print(result_path + curr_datetime + csvPath[csvPath.rfind("/")+1:].replace(".csv", ".json"))

print("Process completed!")

if(save_log_file):
    print("Check log file: " + log_file_name)
library(reshape)
library(dplyr)
library(Seurat)

SetIfIsNull <- function(x, default) {
  if(is.null(x = x)){
    return(default)
  } else {
    return(x)
  }
}

GetHeatmapDataCycle <- function(
  object,
  data.use    = NULL,
  use.scaled  = TRUE,
  cells.use   = NULL,
  genes.use   = NULL,
  disp.min    = -2.5,
  disp.max    = 2.5,
  group.by    = "ident",
  group.order = NULL,
  assay.type  = "RNA"
) {
  
  if (is.null(x = data.use)) {
    if (use.scaled) {
      data.use <- GetAssayData(object,assay.type = assay.type,slot = "scale.data")
    } else {
      data.use <- GetAssayData(object,assay.type = assay.type,slot = "data")
    }
  }
  
  # note: data.use should have cells as column names, genes as row names
  cells.use <- SetIfIsNull(x = cells.use, default = object@cell.names)
  cells.use <- intersect(  x = cells.use, y = colnames(x = data.use))
  if (length(x = cells.use) == 0) {
    stop("No cells given to cells.use present in object")
  }
  
  genes.use <- SetIfIsNull(x = genes.use, default = rownames(x = data.use))
  genes.use <- intersect(x = genes.use, y = rownames(x = data.use))
  if (length(x = genes.use) == 0) {
    stop("No genes given to genes.use present in object")
  }
  
  if (is.null(x = group.by) || group.by == "ident") {
    cells.ident <- object@ident[cells.use]
  } else {
    cells.ident <- factor(x = FetchData(
      object    = object,
      cells.use = cells.use,
      vars.all  = group.by
    )[, 1])
    names(x = cells.ident) <- cells.use
  }
  
  cells.ident <- factor(
    x = cells.ident,
    labels = intersect(x = levels(x = cells.ident), y = cells.ident)
  )
  
  #genes.use = genes.use[]
  #cells.use = cells.use[1:10]
  
  data.use <- data.use[genes.use, cells.use, drop = FALSE]
  if ((!use.scaled)) {
    data.use = as.matrix(x = data.use)
    if (disp.max==2.5) disp.max = 10;
  }
  
  data.use      <- MinMax(data.use, min = disp.min, max = disp.max)
  
  #data.use      <- as.data.frame(x = t(x = data.use))
  
  #data.use.cell <- rownames(data.use)
  
  #colnames(data.use) <- make.unique(colnames(data.use))
  
  #data.use %>% melt(id.vars = "cell") -> data.use
  
  #names(data.use)[names(data.use) == 'variable'] <- 'gene'
  
  #names(data.use)[names(data.use) == 'value'] <- 'expression'
  
  #data.use$ident <- cells.ident[data.use$cell]
  
  # if(!is.null(group.order)) {
  #   if(length(group.order) == length(levels(data.use$ident)) && all(group.order %in% levels(data.use$ident))) {
  #     data.use$ident <- factor(data.use$ident, levels = group.order)
  #   }
  #   else {
  #     stop("Invalid group.order")
  #   }
  # }
  
  #breaks <- seq(
  #  from   = min(data.use$expression),
  #  to     = max(data.use$expression),
  #  length = length(x = PurpleAndYellow()) + 1
  #)
  
  # data.use.gene <- with(
  #   data = data.use,
  #   expr = factor(x = gene, levels = rev(x = unique(x = data.use$gene)))
  # )
  # 
  # data.use.cell <- with(
  #   data = data.use,
  #   expr = factor(x = cell, levels = cells.use)
  # )
  rownames(data.use) <- c()
  colnames(data.use) <- c()
  
  randcycle <- function(x) {
    cycles = c("G1","M","S","G2")
    return(cycles[sample(1:4, 1)])
  }
  
  makel  <- function(x)  { return (c("R", randcycle(x)))}
  listit <- function(x)  { return (c(x) ) }
  
  #test = list(apply(data.use, 1, listit ) )
  
  columns = lapply(cells.use,makel)
  columns = columns[order(sapply(columns,'[[',2))]
  temp_data = list(
    #columns  = lapply(cells.use,makel),
    columns   = columns,
    index    = genes.use,
    data     = data.use
  )
  
  temp_json = jsonlite::toJSON(temp_data, pretty = FALSE)
  temp_json = gsub('\n', '', temp_json)
  #temp_json = gsub('\\', '', temp_json)
  
  return(temp_json)
  
}

import csv

# collect headers into ordered list

test_file = ""

header  = '{"nodes": ['
trailer = ']}'
links   = '],"links": ['

print(header)
linkcount = 0

with open("D:\\Google Drive\\DePaulGraduateSchool\\genomics\\django\\omics\\omics\\static\\omics\\data\\2019129_7_43_37_Bicluster_Filtered_97 _ 419 _Nodes_Adj_Matrix.csv") as f:

    csv_reader = csv.reader(f, delimiter=',')
    line_count = 0
    printedgebefore = False
    for row in csv_reader:
        if line_count == 0:
            #print(f'Column names are {", ".join(row)}')
            namen = row
            line_count += 1
            printbefore = False
            for namegene in namen:
                if len(namegene) > 0:
                    if printbefore:
                        print(",")
                    print('{"id": "' + namegene + '", "group":1}', end="" ),
                    printbefore = True

        else:
            if line_count == 1:
                print('\n' + links)
            #print(row)
            gene_name = row[0]
            row = row[1:len(row)] # drop first column string
            top_gene = 1
            for edge in row:
                #print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
                if int(edge) > 0 :
                    if printedgebefore:
                        print(",")
                    print('{"source":"' + gene_name + '","target":"' + namen[top_gene] + '","value":' + str(edge) + '}', end = "")
                    printedgebefore = True
                top_gene += 1
            line_count += 1

print('\n' + trailer, end="")
# for each row
#   if column > 0
#     output an edge
#
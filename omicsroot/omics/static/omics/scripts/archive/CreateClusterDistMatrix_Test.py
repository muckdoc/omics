# -*- coding: utf-8 -*-
"""
CreateClusterDistMatrix_Test.py

This function calls the create_cluster_wrapper function from the
CreateClusterDistMatrix.py file, which creates the distance matrix for multiple
clusterings, then calls the create_adj_matrixfunction from the same py file to
create the filtered adjacency matrix.  
This is done to test out the use cases of the Python and R scripts in the 
Django framework.

Date      Name              Comment
8/29/18   Kari Palmier      Created
1/9/19    Kari Palmier      Updated to handle PC component and graph component
                            saving removals
                            
"""
#%%
# Read variables from configuration file
from configuration import *
import os
import json

from CreateClusterDistMatrix import create_cluster_wrapper
from CreateClusterDistMatrix import create_adj_matrix
from ConvertAdjToJson import convertAdjToJson


R_dir_path = root_path + '/omicsroot/omics/static/omics/scripts/'

output_rule_path, output_dist_path = create_cluster_wrapper(R_dir_path, 
                                        cluster_data_path, test_types, res_levels, 
                                        sig_level, ngene_up_limit, ngene_low_limit, 
                                        perc_mito_up_limit, perc_mito_low_limit,  
                                        cluster_dims_used, markers_min_pct, 
                                        markers_thresh_use, 
                                        filter_colsums_5000, print_log, 
                                        save_cluster_data, log_to_file)
#%%

adj_filenames = []
node_filenames = []

json_filenames = []

for filter_perc in filter_percs:
    outputs = create_adj_matrix(R_dir_path, filter_perc, output_dist_path, save_graphml, save_hist)
    adj_filenames.append(outputs[0])
    node_filenames.append(outputs[1])
    fname = outputs[0][:outputs[0].rfind("_Adj")] + "_" + str(outputs[2]) + "_Nodes" + outputs[0][outputs[0].rfind("_Adj"):] 
    #convert csv to json
    json_filenames.append(convertAdjToJson(fname))
    
print("Process completed!")
print("Output json files:")
for json_filename in json_filenames:
    print(json_filename)


# %%

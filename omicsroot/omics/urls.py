"""omics URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from django.contrib import admin

from . import views
urlpatterns = [
    url(r'^getJsonFiles',    	     views.getJsonFiles, 	    name='getJsonFiles'),
    url(r'^clusterdata',    	     views.clusterdata, 	    name='clusterdata'),   
    url(r'^ontologydata',    	     views.ontologydata, 	    name='ontologydata'),   
    url(r'^ontospacedata',    	     views.ontospacedata, 	    name='ontospacedata'),       
    url(r'^saveexclusion',    	     views.saveexclusion, 	    name='saveexclusion'),  
    url(r'^workflowresults',	     views.workflowresults, 	name='workflowresults'),
    url(r'^getgodata',	             views.getgodata,       	name='getgodata'),    
    url(r'^reactome',	             views.reactome,       	    name='reactome'),   
    url(r'^genename',	             views.genename,       	    name='genename'),       
    url(r'^fullgo   ',	             views.fullgo,       	    name='fullgo'),    
	url(r'^get_index/$',             views.get_index, 		    name='get_index'),	 
	url(r'^save_index/$',            views.save_index, 		    name='save_index'),    
	url(r'^ajax/prep_submit/$',      views.prep_submit, 		name='prep_submit'),	
	url(r'^ajax/filt_submit/$',      views.filt_submit, 		name='filt_submit'),
	url(r'^ajax/proc_submit/$',      views.proc_submit, 		name='proc_submit'),
    url(r'^ajax/savefam_submit/$',   views.savefam_submit, 	    name='savefam_submit'),
	url(r'^ajax/proc_submit2/$',     views.proc_submit2, 		name='proc_submit'),
    url(r'^ajax/savegene_submit/$',  views.savegene_submit, 	name='savegene_submit'),
    url(r'^astrtree/$',  		 	 views.astrtree, 			name='astrtree'),	    
    url(r'^ldabase',  		 	     views.ldabase, 			name='ldabase'),	
    url(r'^test2',                   views.test2, 			    name='test2'),	
    url(r'^base',                    views.base,                name='base'),
    url(r'^intviz2',                 views.intviz2,             name='intviz2'),  
    url(r'^intviz3',                 views.intviz3,             name='intviz3'),
    url(r'^intviz4',                 views.intviz4,             name='intviz4'),    
    url(r'^intviz5',                 views.intviz5,             name='intviz5'),    
    url(r'^intviz6',                 views.intviz6,             name='intviz6'),   
    url(r'^intviz7',                 views.intviz7,             name='intviz7'),       
    url(r'^intviz8',                 views.intviz8,             name='intviz8'), 
    url(r'^intviz9',                 views.intviz9,             name='intviz9'),    
    url(r'^intviz10',                views.intviz10,            name='intviz10'),
    url(r'^intviz11',                views.intviz11,            name='intviz11'), 
    url(r'^intviz',                  views.intviz,              name='intviz'),  
    url(r'^dials',                   views.dials,               name='dials'),  
    url(r'^vidtest',                 views.vidtest,             name='vidtest'),
    #url(r'^admin/', 			     admin.site.urls),
	url(r'^$',      			     views.index,  			    name='index'),
    #url(r'*',                        views.four,                name='four'),
    url(r'^networkdata',             views.networkdata,         name='networkdata'),
	
]

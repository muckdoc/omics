﻿#----------------------
# updated 05/28/2020
#----------------------


#-----------------
# the base image
#-----------------
FROM r-base:4.0.0


#----------------------
# system dependencies
#----------------------
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
    ## Basic deps
    libcurl4-openssl-dev \
    libssl-dev \
    libhdf5-dev \
    libxml2-dev \
    python3-dev \
    python3-pip \
    ## editors
    nano


#------------
# ADD files
#------------
ADD /omicsroot/ /omicsroot
ADD /startExperiment.sh /startExperiment.sh
ADD /editConfiguration.sh /editConfiguration.sh
ADD /omicsroot/omics/static/omics/scripts/SDMTools_1.1-221.2.tar.gz /omicsroot/omics/static/omics/scripts/


#----------------------
# install R libraries, and BiocManager R libraries using install()
#----------------------
RUN R -e "install.packages('BiocManager')"
RUN R -e "BiocManager::install('multtest', update = FALSE)"
RUN R -e "BiocManager::install('mygene', update = FALSE)"
RUN R -e "BiocManager::install('intergraph', update = FALSE)"
RUN R -e "install.packages('reshape')"

#### seurat v2
RUN R -e "install.packages('R.utils')"
RUN R -e "install.packages('/omicsroot/omics/static/omics/scripts/SDMTools_1.1-221.2.tar.gz', repos=NULL, type='source')"
RUN R -e "source('https://z.umn.edu/archived-seurat')"
####

#-------------------------
# install python packages
#-------------------------
RUN pip3 install django
RUN pip3 install django-cors-headers
RUN pip3 install requests
RUN pip3 install psutil
RUN pip3 install pandas


#-------------------
# run django server
#-------------------
CMD ["python3", "/omicsroot/manage.py", "migrate"]
CMD ["python3", "/omicsroot/manage.py", "runserver", "0.0.0.0:8000"]


#--------------
# expose port
#--------------
EXPOSE 8000



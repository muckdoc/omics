from django.shortcuts     import render
from django.http          import HttpResponse
from django.template      import loader
from django.http          import JsonResponse
from django.conf         import settings

from django.http import JsonResponse
from django.utils.encoding import smart_str
from django.views.decorators.csrf import csrf_protect

import subprocess
import shlex
import os
import logging
import datetime
import random
import json
import time
import re
import requests
from glob import glob
#import urllib.request

#rpath      = '"D:\\R\\R-3.4.2\\bin\\rscript"'
#scriptpath = 'D:\\Google Drive\\DePaulGraduateSchool\\genomics\\django\\omics\\omics\\templates\\omics\\'

logger = logging.getLogger('django')
# #logger.setLevel(logging.DEBUG)
print(logger)
logger.warning('Watch out!')
#logger.warning(__name__)
# logging.basicConfig(
    # level = logging.DEBUG,
    # format = '%(name)s %(levelname)s %(message)s',
# )

# load R session fields here!



def clusterdata(request):
    print(request)
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    cluster = request.GET.get('cluster')
    print(cluster)
    view = request.GET.get('view')
    print(view)
    print(settings.BASE_PATH)
    print(os.path.dirname(__file__))
    if(view == 'express'):
        # get the cluster data for this cluster
        jsonccpath     = os.path.join(settings.BASE_PATH,'static','omics','data','heatdata.json')
        logging.warning(jsonccpath)
        scriptname = 'heatwrapper2.R'
        with open(jsonccpath,'r') as json_data:
            logger.warning('trying to open file')
            data = json.load(json_data)
        return(JsonResponse(data,safe=False))         
    else:
        if(view =='cc'):
            excludepath     = os.path.join(settings.BASE_PATH,'static','omics','data','heatdata.json')
            logging.warning(excludepath)
            scriptname = 'heatwrapper.R'
            
        #with open(excludepath,'r') as excludes:
            #excludes = json.load(excludes)
            #print(excludes)    

            #print(settings.BASE_PATH)
            #subcommand = os.path.join(settings.BASE_PATH,'static/omics/data/'+'heatdata.json')
            #subcommand = 'cd ' + '"' +os.path.join(settings.BASE_PATH,'templates','omics')  + '"' +'\n' # windows specific
    subcommand = ''
    print(settings.BASE_PATH)
    print(settings.RPATH)
    result  = re.search('\s',settings.BASE_PATH)
    result2 = re.search('\s',settings.RPATH)
    
    print(result)
    print(result2)
    if(result or result2):
        subcommand = subcommand + settings.RPATH + ' ' + '"' + os.path.join(settings.BASE_PATH,'templates','omics',scriptname).replace("\\","/") + '" ' + cluster
    else:    
        subcommand = subcommand + settings.RPATH + ' ' + os.path.join(settings.BASE_PATH,'templates','omics',scriptname).replace("\\","/") +' ' + cluster            
    logging.warning(subcommand)
    print(subcommand)
    try:
        if(result or result2):
            argstoUse = subcommand
        else:
            argstoUse = shlex.split(subcommand)            
        p = subprocess.check_output(argstoUse)    
        #print(p)
        p_lines = p.decode().split('\r\n')
        #p_lines = p
        #print(p_lines)
        outtest = os.path.join(settings.BASE_PATH, 'static/omics/data/' + 'logjson.out')
        #print(outtest)
        with open(outtest,'w') as outtest:
            for line in p_lines:
                outtest.write(line)
        intest = os.path.join(settings.BASE_PATH, 'static/omics/data/' + 'logjson.out')
        with open(intest) as json_data:
            logger.warning('trying to open file')
            data = json.load(json_data)
   
        return(JsonResponse(data,safe=False));
    except subprocess.CalledProcessError as e:
        print ("Process:\n" + e.output.decode())        

# send data back for tree        
def ontologydata(request):
    cluster = request.GET.get('cluster')
    print(cluster)
    view = request.GET.get('view')
    print(view)
    jsonccpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'gotree.json')
    logging.warning(jsonccpath)
    with open(jsonccpath,'r') as json_data:
        logger.warning('trying to open ontology tree file')
        data = json.load(json_data)
    return(JsonResponse(data,safe=False));  

def getJsonFiles(request):
    # use relative path here
    jsonListOfFileNames = [os.path.basename(f).replace(".json", "") for f in glob('omicsroot/omics/static/omics/results/*_Nodes_Adj_Matrix.json')]
    return(JsonResponse(jsonListOfFileNames,safe=False))
    

def ontospacedata(request):
    #n.GET.get('genelist')
    json_list = request.body.decode("utf-8")    
    print(json_list)    
    test = True
    jsonpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'genelist.json')
    logging.warning(jsonpath)
    with open(jsonpath,'w') as json_data:
        json_data.write(json_list)
        
    if(test):
        jsonccpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'ontospacedata.json')
        logging.warning(jsonccpath)
        with open(jsonccpath,'r') as json_data:
            logger.warning('trying to open ontology tree file')
            data = json.load(json_data)    
    return(JsonResponse(data,safe=False));             
    
def getgodata(request):
    #genelist = request.GET.get('genelist')
    json_list = request.body.decode("utf-8")    
    print(json_list)
    
    test = True
    if(test):
        scriptname = 'GetGeneInfoBatch.R'
        subcommand = ''
        subcommand = subcommand + settings.RPATH + ' ' + '"' + os.path.join(settings.BASE_PATH,'templates','omics',scriptname).replace("\\","/") + '" ' 
        print(subcommand)   
        #p = subprocess.check_output(subcommand)
        jsonccpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'GOdata.json')
        logging.warning(jsonccpath)
        with open(jsonccpath,'r') as json_data:
            logger.warning('trying to open ontology tree file')
            data = json.load(json_data)
    #else:
    #    # write this out and call the R function to get the list back
        
        
        
    return(JsonResponse(data,safe=False)); 
    
def reactome(request):
    print('reactome')
    gene = request.GET.get('gene')
    r    = requests.get('https://reactome.org/ContentService/search/query?query=' + gene + '&species=Homo+sapiens&types=Pathway&cluster=true')
    jsondata = r.json()
    
    #urlData = 'https://reactome.org/ContentService/search/query?query=' + gene + '&species=Homo+sapiens&types=Pathway&cluster=true'
    #try:
    #    webURL = urllib.request.urlopen(urlData)       
    #    data = webURL.read()
    #    print(data)
    #    encoding = webURL.info().get_content_charset('utf-8')
    #    jsondata = json.loads(data.decode(encoding))
    #    #JSON_object = json.loads(webURL.read())
    #    #print(json)
    return(JsonResponse(jsondata,safe=False))
    #except:
    #    print('not found')    
    #    #print (webURL)
    #    return(JsonResponse('No Reactome Results',safe=False))

def genename(request):
    gene = request.GET.get('gene')
    url  = 'https://rest.ensembl.org/lookup/symbol/homo_sapiens/' + gene + '?content-type=application/json;expand=1'
    print(url)
    r    = requests.get(url)
    jsondata = r.json()
    return(JsonResponse(jsondata,safe=False));
    
def fullgo(request):
    gene = request.GET.get('gene')
    # gene must be ensembl ID
    url  = 'http://mygene.info/v3/gene/' + gene 
    print(url)
    r    = requests.get(url)
    jsondata = r.json()
    return(JsonResponse(jsondata,safe=False));
    
def astrtree(request):

    #week = request.GET.get('week')
    #print(week)
    #view = request.GET.get('view')
    #print(view)
    jsonccpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'tree1.html')
    logging.warning(jsonccpath)
    print(jsonccpath)
    with open(jsonccpath,'r') as f:
        if f:
            print(f)
            logger.warning(f)
            logger.warning('trying to open astr tree file')
            data = f.read()
            logger.warning(data)
        else:
            print('cannot open file')
            logger.warning('file not found')
    return(HttpResponse(data))        
    #return(JsonResponse(data,safe=False));      
def zereocountdata(request):
    return(JsonResponse(data,safe=False));
    
def savefam_submit(request):
    data = { 'save fam called': True }
    return JsonResponse(data)
    
def savegene_submit(request):
    data = { 'save gene called': True }
    return JsonResponse(data)
    
def prep_submit(request):
    # Get an instance of a logger
    logger = logging.getLogger(__name__)
  
    #call workflow here
    #logger.warning('before calling preprocess')
    p       = subprocess.run('workflowprep.bat', stdout = subprocess.PIPE)
    #logger.warning('after calling preprocess')
    #logger.warning(type(p.stdout))
    p_lines = p.stdout.decode().split('\r\n')
    #for row in output.split('\n'):
    #p_lines = p.stdout
    #logger.warning(p_lines)
    #data = {}
    linenum = 1
    data = {'response':p.stdout.decode()}
    for line in p_lines:
        logger.warning(line)
        data['"'+str(linenum)+'"'] = str(line)
    #    #logger.warning(data)
        linenum = linenum + 1
    #data = data + '}'
    #logger.warning(data)
    return JsonResponse(data)

def four(request):
        return HttpResponse("Welcome to RaspberryPi")
        
def get_index(request):
        dataout = { "index":"0"}
        # once pulled should be cleared
        indexpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'indexthing.json')
        # read item
        logger.warning(indexpath)
        with open(indexpath) as datain:
            json_data = json.load(datain)
        logger.warning(json_data)
        # write item
        with open(indexpath,'w') as data:
            json.dump(dataout,data)
        return JsonResponse(json_data)
        
def save_index(request):
        data = { "index":"1"}
        indexpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'indexthing.json')   
        with open(indexpath, 'w') as outfile:  
            json.dump(data, outfile)
        return JsonResponse(data)        
    
def filt_submit(request):
    data = { 'is_taken': True }
    return JsonResponse(data)
    
def proc_submit2(request):
    time.sleep(6)
    data = { 'complete': True }
    return JsonResponse(data)        
        
def proc_submit(request):
    # Get an instance of a logger
    logger = logging.getLogger(__name__)
    #logger.warning(settings.STATIC_URL)
    #logger.warning(settings.STATIC_URL+'omics'+'/images/')
    #logger.warning(settings.BASE_DIR+'omics'+'/images/', 'histcolstats.jpg')
    logger.warning(str(random.randint(1,1000000)))
    histcolstats  = request.build_absolute_uri(settings.STATIC_URL+'omics/images/histcolstats.jpg'+'?random='+str(random.randint(1,1000000)))  
    histrowstats  = request.build_absolute_uri(settings.STATIC_URL+'omics/images/histrowstats.jpg'+'?random='+str(random.randint(1,1000000)))    
    heatmapfile   = request.build_absolute_uri(settings.STATIC_URL+'omics/images/familyheat.jpg'+'?random='+str(random.randint(1,1000000)))    
   
    #logger.warning(histcolstats)
    #histcolstats = os.path.join(settings.BASE_DIR+'omics\\'+'/images/', 'histcolstats.jpg')
    #print(settings.STATIC_ROOT)
    
    #call workflow here
    p = subprocess.run('workflow.bat', shell=False)
    #p.wait
    jsonfampath     = (settings.BASE_DIR+'\\omics\\static\\omics\\data\\'+'fammap.json')
    logger.warning(jsonfampath)

    with open(jsonfampath) as json_data:
        jsonfam = json.load(json_data)

    data = { 'is_taken' : True,
    'history' : ['step1','step2','step3','step4','step5'],
    'famdata' : jsonfam,
    'histcolfile'  : histcolstats,
    'histrowfile'  : histrowstats,
    'heatmapfile'  : heatmapfile
    }
    return JsonResponse(data)

def saveexclusion(request):
    logger.warning('save exclusion!')
    jsonccpath     = os.path.join(settings.BASE_PATH,'static/omics/data/'+'exclusion.json')    
    with open(jsonccpath,'w') as json_data:
        logger.warning('trying to open file')
        json_send = request.body.decode("utf-8")
        print(json_send);        
        print(json_send)
        json_data.write(json_send)
    return HttpResponse("Hello, world. ")       

def index(request):
    #return HttpResponse("Hello, world. ")
    template = loader.get_template('omics/index.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    
    return HttpResponse(template.render({'family': family},request))

def base(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)
    logger.warning('Watch out!')    
    template = loader.get_template('omics/base.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    
    return HttpResponse(template.render({'family': family},request))
    
def intviz(request):
    #return HttpResponse("Hello, world. ")
    template = loader.get_template('omics/intviz.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    
    return HttpResponse(template.render({'family': family},request))

def intviz2(request):
    #return HttpResponse("Hello, world. ")
    template = loader.get_template('omics/intviz2.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    
    return HttpResponse(template.render({'family': family},request))
    
def intviz3(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz3')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz3')

    template = loader.get_template('omics/intviz3.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    
    return HttpResponse(template.render({'family': family},request))    
    
def ldabase(request):


    template = loader.get_template('omics/ldabase.html')
    # termlist = {  'terms' : {'XTANDI' : {'top1':123,'top2':123,'top3':123},
                             # 'anemia' : {'top1':123,'top2':123,'top3':123},
                             # 'Enzalutimide' :{'top1':123,'top2':123,'top3':123},
                             # 'Fatigue':{'top1':123,'top2':123,'top3':123},
                             # } };
  
    termlist = {  'terms':{'XTANDI': { 'top1':'123',
                                       'top2':'234'
                            }}  
                };
                             
    return HttpResponse(template.render(termlist,request))   
    
def intviz4(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz4')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz4')

    template = loader.get_template('omics/intviz4.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))       
    
def intviz5(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz5')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz5')

    template = loader.get_template('omics/intviz5.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))    
    
def intviz6(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/intviz6.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))        
    
def intviz7(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/intviz7.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))      
    
def intviz8(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/intviz8.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))      
    
def intviz9(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/intviz9.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))          
    
    
def intviz10(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz10')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz10')

    template = loader.get_template('omics/intviz10.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))        
    
def intviz11(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz11')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz11')

    template = loader.get_template('omics/intviz11.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))
    
def dials(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/dials.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))     
    
def vidtest(request):
    #return HttpResponse("Hello, world. ")
    logger = logging.getLogger('django')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')
    
    logger = logging.getLogger('omics')
    #logger.setLevel(logging.DEBUG)
    print(logger)    
    logger.warning('intviz6')

    template = loader.get_template('omics/vidtest.html')
    family = [
        'family1',
        'family2',
        'family3',
        'family4',
    ]
    return HttpResponse(template.render({'family': family},request))      
    
def workflowresults(request):
    template = loader.get_template('omics/workflowresults.html')
    return_code = subprocess.call('D:\R\R-3.3.1\bin\rscript "D:\\Google Drive\\DePaulGraduateSchool\\genomics\\AnalysisWorkflow-test.R"', shell=False)
    content = {
        'latest_question_list': '',
    }
    return HttpResponse(template.render(content,request))
    
def test2(request):
    template = loader.get_template('omics/test2.html')
    #return_code = subprocess.call('workflow.bat', shell=False)
    content = {
        'latest_question_list': '',
    }
    return HttpResponse(template.render(content,request))    
    
def baseKeep(request):
    template = loader.get_template('omics/base.html')   
    import json
    f = open('C:\\Users\\doug\\Documents\\CoffeeCup Software\\My Website\\experiment1.json', 'r')
    thingy = json.load(f)

    meta = thingy[0]

    html = ''
    html = '<p>thingy here</p>'
    #html = html + '<div id="container" role="main">\n' +  '<div id="jstree">\n'
    # print(html)
    # print('<ul>')
    # print('<li id = "child_{0}"> {0}'.format(meta['experiment']))
    # print('<ul>')
    # #print(meta['experiment'])
    # for steps in sorted(meta['steps']):

        # for step in sorted(steps):
            # #print(step)
            # stepdetail = steps[step]
            # print('<li id="child_{0}_{1}">'.format(meta['experiment'],step),end='')
            # #print(stepdetail)
            # #print()
            # stepcontents = stepdetail[0]
            # print(stepcontents['name'],end='')
            # resultlist = stepcontents['results'][0]
            # #resultlistdetail = resultlist[]
            # #print(resultlist)
            # list = 0
            # for thing in sorted(resultlist):
                # #print(thing,end='')
                # #print(':',end='')
                # #print(resultlist[thing])
                # if('graphic' in thing):
                    # if(list == 0):
                        # list = 1
                        # print('\n<ul>')
                    # print('<li id="child_{0}_{1}_{2}">{2}</li>'.format(meta['experiment'], step,thing))
            # if(list > 0):
                # print('\n</ul>')
            # print('\n</li>')

        # print('</li>')

    # print('</ul>')

    # print('</div>')
    # print('<div id="data">')
    # print('<div class="content code" style="display:none;"><textarea id="code" readonly="readonly"></textarea></div>')
    # print('<div class="content folder" style="display:none;"></div>')
    # print('<div class="content image" style="display:none; position:relative;"><img src="" alt="" style="display:block; ',end='')
    # print ('position:absolute; left:50%; top:50%; padding:0; max-height:90%; max-width:90%;" /></div>')
    # print('<div class="content default" style="text-align:center;">No Results Selected</div>')
    # print('</div>')

    # print('</div>\n')

    # for steps in sorted(meta['steps']):

        # # print(steps)

        # # generate top level information
        # print('$(\'#jstree\').on("changed.jstree", function (e, data) {')
        # print('console.log(data.selected);')
        # print('var')
        # print('i, j, r = [];')
        # print('i = 0')
        # print('$(\'#data\').html(data.instance.get_node(data.selected[i]).text);')
        # print('var thing = data.instance.get_node(data.selected[i])')
        # print('if (thing.id === "child_{0}") '.format(meta['experiment']),end='')
        # print('{')
        # print('$(\'#data\').html(\'<table id="resultstab" ></table>\')')
        # for thing in sorted(meta):
            # if(thing != 'steps'):
                # print('$(\'#resultstab\').append(\'<tr ><td>{0}</td><td>{1}</td></tr>\')'.format(thing,meta[thing]));
        # print('}')

        # for step in sorted(steps):
            # #print( step)
            # print('if (thing.id === "child_{0}_{1}") '.format(meta['experiment'],step), end='')
            # print('{')
            # print('$(\'#data\').html(\'<table id="resultstab" ></table>\')')
            # #print(steps[step][0])
            # items = steps[step][0]
            # results = items['results'][0]
            # for thing in sorted(results):
                # #print(thing)
                # if 'graphic' not in thing:
                    # print('$(\'#resultstab\').append(\'<tr ><td>{0}</td><td>{1}</td></tr>\')'.format(thing, results[thing]))
            # print('}')
            # for thing in sorted(results):
                # if 'graphic' in thing:
                    # print('if (thing.id === "child_{0}_{1}_{2}") '.format(meta['experiment'], step,thing), end='')
                    # print('{')
                    # print('$(\'#data\').html(\'<img src="{0}" />\')'.format(results[thing]))
                    # print('}')
                    # #print('$(\'#data\').html(\'<table id="resultstab" ></table>\')')

        # print('});')

    # print('});')
    # print('</script>')
    # print('</body></html>')   
    return HttpResponse(template.render({'family': html},request))    
    #return(HttpResponse(html))
        
def networkdata(request):
    logger = logging.getLogger('django')
    # logger.warning("------network data")
    print(logger)
    logger.warning(request)

    fileName = request.GET.get('file')
    jsonFileName = fileName+'.json'
    filepath = os.path.join(settings.BASE_PATH,'static/omics/results/' + str(jsonFileName))
    # logger.warning("---file path")
    # logger.warning(filepath)
    # logger.warning("---file name")
    # logger.warning(fileName)
    # logger.warning("---json name")
    # logger.warning(jsonFileName)
    # logger.warning(type(jsonFileName))
    # logger.warning("---end---network data")


    response = HttpResponse(open(filepath, 'r'),content_type='application/force-download') 
    response['Content-Disposition'] = 'attachment; filename='+ os.path.basename(filepath)
    return response


    
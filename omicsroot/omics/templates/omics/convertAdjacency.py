

import re
import csv
import random
import math

counter = 0 # need each line to have a unique key

# output json starter section

# read csv
with open('D:/Google Drive/DePaulGraduateSchool/genomics/django/omics/omics/static/omics/data/2018715_21_44_53_Bicluster_Adjacency_Low75.csv','r') as insights:
    # create vector from first row
    spamreader = csv.reader(insights, delimiter=',', quotechar='"')
    with open('D:/Google Drive/DePaulGraduateSchool/genomics/django/omics/omics/static/omics/data/adjacency.json','w') as combinedout:
        rowid = 0
        print('{"nodes": [')
        combinedout.write('{"nodes": [\n')
        wrote_edge = False
        for row in spamreader:
            if rowid == 0:
                # output node list JSON
                nodes = row[1:]
                rowid+=1
                #print(nodes)
                for ind in range(0,len(nodes)):
                    #print(ind)
                    #print(nodes[ind])
                    randgroup =  random.randrange(1,8,1)
                    node = '{"id": "' + nodes[ind] + '", "group":'+ str(randgroup) +'}'
                    if ind < len(nodes)-1:
                        node = node + ','
                    print(node)
                    combinedout.write(node+'\n')
                print('],')
                combinedout.write('],')
            else:
                # output edges
                #print(row)
                #print(len(row))
                source = row[0]
                #print(source)
                if rowid == 1:
                    print('"links": [')
                    rowid+=1
                    combinedout.write('"links": ['+'\n')
                #else:
                    #print(',')
                    #combinedout.write(',\n')
                for ind in range(0,len(row[1:])):
                    #print(ind)
                    #print(nodes[ind])
                    if(int(row[ind+1]) > 0):
                        #a = 1
                        #b =
                        #scaled_val = (((b-a)*(x-min)) / (max-min)) + a

                        #edge = '{"source":'+'"'+ source + '",' + '"target":' + '"' + nodes[ind] +'","value":' + str(int(math.exp(int(row[ind+1]))/1000)) + '}'
                        edge = '{"source":'+'"'+ source + '",' + '"target":' + '"' + nodes[ind] +'","value":' + row[ind+1] + '}'

                        if wrote_edge:
                            #edge = edge + ','
                            print(',')
                            combinedout.write(',\n')
                        else:
                            wrote_edge = True
                        print(edge)
                        combinedout.write(edge)
                    else:
                        print(int(row[ind+1]))
        print(']}')
        combinedout.write('\n]}')
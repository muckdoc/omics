import pandas as pd
import json

def convertAdjToJson(csvPath):
    jsonPath = "/omicsroot/omics/static/omics/results/" + csvPath[csvPath.rfind("/")+1:].replace(".csv", ".json")
    df = pd.read_csv(csvPath, index_col = 0, header = 0)

    outputdata = {}
    outputdata["nodes"] = []
    outputdata["links"] = []

    for col in df.columns:
        outputdata["nodes"].append({"id":col,"group":1})
        for row in df.index:
            if df.loc[row, col] != 0:
                outputdata["links"].append({"source":col,"target":row,"value":int(df.loc[row, col])})
    
    with open(jsonPath, 'w') as outfile:
        json.dump(outputdata, outfile)
    
    return jsonPath
